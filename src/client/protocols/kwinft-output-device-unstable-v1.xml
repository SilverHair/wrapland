<?xml version="1.0" encoding="UTF-8"?>
<protocol name="kwinft_output_device_unstable_v1">
    <copyright><![CDATA[
        Copyright © 2015 Sebastian Kügler <sebas@kde.org>
        Copyright © 2020 Roman Gilg <subdiff@gmail.com>

        Permission is hereby granted, free of charge, to any person obtaining a
        copy of this software and associated documentation files (the "Software"),
        to deal in the Software without restriction, including without limitation
        the rights to use, copy, modify, merge, publish, distribute, sublicense,
        and/or sell copies of the Software, and to permit persons to whom the
        Software is furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included
        in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
        THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
        FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
        DEALINGS IN THE SOFTWARE.
    ]]></copyright>

    <description summary="protocol to obtain information about output devices">
      This protocol exposes an interface for globally announced output devices.

      Warning! The protocol described in this file is experimental and
      backward incompatible changes may be made.
    </description>

    <interface name="zkwinft_output_device_v1" version="1">
        <description summary="output configuration representation">
            An output device describes a display device available to the compositor.
            output_device is similar to wl_output, but focuses on output
            configuration management.

            A client can query all global output_device objects to enlist all
            available display devices, even those that may currently not be
            represented by the compositor as a wl_output.

            The client sends configuration changes to the server through the
            output_configuration interface, and the server applies the configuration
            changes to the hardware and signals changes to the output devices
            accordingly.

            This object is published as global during start up for every available
            display devices, or when one later becomes available, for example by
            being hotplugged via a physical connector.
        </description>

        <event name="info">
            <description summary="general information about this output">
                The info event describes some base information about the output.

                The arguments make and model should be the same as in the wl_output
                interface. Together with the serial number they should be consistent
                over compositor restarts such that clients are able to identify outputs
                from previous sessions.

                The event is sent when binding to the output object and whenever
                any of the properties change.
            </description>
            <arg name="name" type="string"
                 summary="unique output identifier"/>
            <arg name="description" type="string"
                 summary="human-readable pre-translated device description"/>
            <arg name="make" type="string"
                 summary="textual description of the manufacturer"/>
            <arg name="model" type="string"
                 summary="textual description of the model"/>
            <arg name="serial_number" type="string"
                 summary="serial number of the product"/>
            <arg name="physical_width" type="int"
                 summary="width in millimeters of the output"/>
            <arg name="physical_height" type="int"
                 summary="height in millimeters of the output"/>
        </event>

        <enum name="enablement">
            <description summary="describes enabled state">
                Describes whether a device is enabled, i.e. device is used to
                display content by the compositor. This wraps a boolean around
                an int to avoid a boolean trap.
            </description>
            <entry name="disabled" value="0"/>
            <entry name="enabled" value="1"/>
        </enum>

        <event name="enabled">
            <description summary="output is enabled or disabled">
                The enabled event notifies whether this output is currently
                enabled and used for displaying content by the server.
                The event is sent when binding to the output object and
                whenever later on an output changes its state by becoming
                enabled or disabled.
            </description>
            <arg name="enabled" type="int" summary="output enabled state"/>
        </event>

        <enum name="mode_flag" bitfield="true">
            <description summary="mode information">
                These flags describe properties of an output mode. They are
                used in the flags bitfield of the mode event.
            </description>
            <entry name="current" value="0"
                   summary="indicates this is the current mode"/>
            <entry name="preferred" value="1"
                   summary="indicates this is the preferred mode"/>
        </enum>

        <event name="mode">
            <description summary="advertise available output modes and current one">
                The mode event describes an available mode for the output.

                When the client binds to the output_device object, the server sends this
                event once for every available mode the output device can be operated by.

                There will always be at least one event sent out on initial binding,
                which represents the current mode.

                Later on if an output changes its mode the event is sent again, whereby
                this event represents the mode that has now become current. In other
                words, the current mode is always represented by the latest event sent
                with the current flag set.

                The size of a mode is given in physical hardware units of the output device.
                This is not necessarily the same as the output size in the global compositor
                space.

                The id can be used to refer to a mode when calling set_mode on an
                zkwinft_output_configuration_v1 object.
            </description>
            <arg name="flags" type="uint" summary="bitfield of mode flags" enum="mode_flag"/>
            <arg name="width" type="int" summary="width of the mode in hardware units"/>
            <arg name="height" type="int" summary="height of the mode in hardware units"/>
            <arg name="refresh" type="int" summary="vertical refresh rate in mHz"/>
            <arg name="mode_id" type="int"
                 summary="Per output_device unique id to identify a mode"/>
        </event>

        <enum name="transform">
            <description summary="transform from framebuffer to output">
                This describes the transform, that a compositor will apply to a
                surface to compensate for the rotation or mirroring of an
                output device.

                The flipped values correspond to an initial flip around a
                vertical axis followed by rotation.

                The purpose is mainly to allow clients to render accordingly and
                tell the compositor, so that for fullscreen surfaces, the
                compositor is still able to scan out directly client surfaces.
            </description>

            <entry name="normal" value="0"/>
            <entry name="90" value="1"/>
            <entry name="180" value="2"/>
            <entry name="270" value="3"/>
            <entry name="flipped" value="4"/>
            <entry name="flipped_90" value="5"/>
            <entry name="flipped_180" value="6"/>
            <entry name="flipped_270" value="7"/>
        </enum>

        <event name="transform">
            <description summary="output transformation">
                This event describes the current transformation of the output, which determines how
                the rectangle send in the geometry will be transformed when being put out on the
                physical screen.

                The event is sent when binding to the output object and whenever
                the transformation changes.
            </description>
            <arg name="transform" type="int" enum="transform"/>
        </event>

        <event name="geometry">
            <description summary="output geometry in compositor space">
                The geometry event describes the logical position and size of the output in
                compositor space.

                The width and height argument are  take the current transform into account. This
                means the rectangle defined by x, y, width, height will be transformed according to
                the transform value before put out on the output.

                The event is sent when binding to the output object and whenever
                any of the properties change.
            </description>
            <arg name="x" type="fixed"
                 summary="x position within the global compositor space"/>
            <arg name="y" type="fixed"
                 summary="y position within the global compositor space"/>
            <arg name="width" type="fixed"
                 summary="logical width of the output in global compositor space"/>
            <arg name="height" type="fixed"
                 summary="logical height of the output in global compositor space"/>
        </event>

        <event name="done">
            <description summary="sent all information about output">
                This event is sent after all other properties have been
                sent on binding to the output object as well as after any
                other output property change have been applied later on.
                This allows to see changes to the output properties as atomic,
                even if multiple events successively announce them.
            </description>
        </event>

    </interface>
</protocol>
